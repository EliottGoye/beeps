#!/bin/bash
        for i in {0..3}
        do
            echo -en "\E[$[i+$1];$[$2+m]H\E[44m${S[0]}"
        done
        : $[m-=5] # move the next frame 5 columns to the left
    done
}
# count star positions according to nyan cat position 
# -3,+3 best constant, I've played with those numbers
# to get best position of stars and do not destroy the cat :)
my=$[(Y-11)/2 - 3]
My=$[my+11 + 3]
while true
do
    for i in $(eval echo {1..$my..4}) # each position 4 rows difference
    do
        # draw top star, animation has 20 columns length, so generated
        # for lowest start position columns is 0+20
        (s $i $[20 + $RANDOM % (X - 20)]) &
        # draw bottom star 
        (s $[i+My] $[20 + $RANDOM % (X - 20)]) &
    done
    sleep 0.5
done &
######################
### Draw rainbow flag
######################
# oOPXxUuT .. top wave, OePxdUTc bottom wave
A="oOPXxUuTOePxdUTc";
# Flag waving:
# $c .. flip flop when counter $i modulo 5 is zero,
#       then it adress top or bottom wave
while true
do
    : $[c^=1]
    for x in `eval echo {1..$[xp]}`
    do
        if [ $[i++ % 5] -eq 4 ]; then : $[c^=1]; fi;
        for y in {0..7}
        do
            echo -en "\E["$[yp+y+2]";${x}H\E[${B[${A:$[y+(c*8)]:1}]}\E[0m"
        done
    done
    sleep 0.5
done